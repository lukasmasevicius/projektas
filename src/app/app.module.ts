import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UrlsComponent } from './urls/urls.component';
import { UrlListComponent } from './urls/url-list/url-list.component';
import { UrlStartComponent } from './urls/url-start/url-start.component';
import { UrlItemComponent } from './urls/url-list/url-item/url-item.component';
import { UrlEditComponent } from './urls/url-edit/url-edit.component';
import { UrlDetailComponent } from './urls/url-detail/url-detail.component';
import { AuthComponent } from './auth/auth.component';
import { UrlService } from './urls/url.service';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { HeaderComponent } from './header/header.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    AppComponent,
    UrlsComponent,
    UrlListComponent,
    UrlStartComponent,
    UrlItemComponent,
    UrlEditComponent,
    UrlDetailComponent,
    AuthComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NoopAnimationsModule,
    MatPaginatorModule
  ],
  providers: [
    UrlService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap, take, exhaustMap } from 'rxjs/operators';


import { AuthService } from '../auth/auth.service';
import { Url } from '../urls/url.model';
import { UrlService } from '../urls/url.service';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(
    private http: HttpClient,
    private urlService: UrlService,
    private authService: AuthService
  ) {}

  storeURLs() {
    const urls = this.urlService.getURLs();
    this.http
      .put(
        'https://http-requests-angular-3c70b-default-rtdb.firebaseio.com/urls.json',//@TODO Change with created EndPoints
        urls
      )
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchURLs() {
    return this.http
      .get<Url[]>(
        'https://http-requests-angular-3c70b-default-rtdb.firebaseio.com/urls.json'//@TODO Change with created EndPoints
      )
      .pipe(
        map(urls => {
          return urls.map(url => {
            return {
              ...url
            };
          });
        }),
        tap(url => {
          this.urlService.setURLs(url);
        })
      );
  }
}

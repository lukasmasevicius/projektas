import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { UrlService } from '../url.service';

@Component({
  selector: 'app-url-edit',
  templateUrl: './url-edit.component.html',
  styleUrls: ['./url-edit.component.css']
})
export class UrlEditComponent implements OnInit {
  id: number = 0;
  editMode = false;
  urlForm!: FormGroup;
  success : boolean = false;

  constructor(
    private route: ActivatedRoute,
    private urlService: UrlService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  onSubmit() {

    this.urlService.addURL(this.urlForm?.value);  //@TODO After submit pagination is shown not correctly
    this.success = true;

    
  }

  onCloseSuccess()
  {
    this.success = false;
    this.onCancel();
  }
  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
    
  }

  private initForm() {
    let originalPath = 'yourURL.com/details';
    const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

    this.urlForm = new FormGroup({
      originalPath: new FormControl(originalPath, [Validators.required, Validators.pattern(urlRegex)]),
    });
  }
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Url } from './url.model';

@Injectable()
export class UrlService {
  urlsChanged = new Subject<Url[]>();

  private urls: Url[] = [
    new Url(
      'Test123.lt',
      'testtestetst.com'  ),
    new Url(
        'REST665.lt',
        'restrestrest.com'  ),
    
  ];

  constructor() {}

//@TODO Insert Created Endpoints
//  private urls: Url[] = []
//   getAll(params: any): Observable<any> {
//     return this.http.get<any>(baseUrl, { params });
//   }

//   get(id: any): Observable<Tutorial> {
//     return this.http.get(`${baseUrl}/${id}`);
//   }

//   create(data: any): Observable<any> {
//     return this.http.post(baseUrl, data);
//   }

//   update(id: any, data: any): Observable<any> {
//     return this.http.put(`${baseUrl}/${id}`, data);
//   }

//   delete(id: any): Observable<any> {
//     return this.http.delete(`${baseUrl}/${id}`);
//   }
  setURLs(urls: Url[]) {
    this.urls = urls;
    this.urlsChanged.next(this.urls.slice());
  }

  getURLs() {
    return this.urls.slice();
  }

  getURL(index: number) {
    return this.urls[index];
  }

  addURL(url: Url) {
    this.urls.push(url);
    this.urlsChanged.next(this.urls.slice());
  }

  updateURL(index: number, newUrl: Url) {
    this.urls[index] = newUrl;
    this.urlsChanged.next(this.urls.slice());
  }

  deleteURL(index: number) {
    this.urls.splice(index, 1);
    this.urlsChanged.next(this.urls.slice());
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { Url } from '../../url.model';

@Component({
  selector: 'app-url-item',
  templateUrl: './url-item.component.html',
  styleUrls: ['./url-item.component.css']
})
export class UrlItemComponent implements OnInit {
  @Input() url!: Url;
  @Input() index?: number;


  ngOnInit(): void {
  }

}

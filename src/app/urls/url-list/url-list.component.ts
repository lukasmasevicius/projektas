import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Url } from '../url.model';
import { UrlService } from '../url.service';

@Component({
  selector: 'app-url-list',
  templateUrl: './url-list.component.html',
  styleUrls: ['./url-list.component.css']
})
export class UrlListComponent implements OnInit, OnDestroy {
  urls!: Url[];
  subscription?: Subscription;

  constructor(private urlService: UrlService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  lowValue: number = 0;
  highValue: number = 20;

  ngOnInit() {
    this.subscription = this.urlService.urlsChanged
      .subscribe(
        (urls: Url[]) => {
          this.urls = urls;
        }
      );
    this.urls = this.urlService.getURLs();
  }

  public getPaginatorData(event: PageEvent): PageEvent {
    this.lowValue = event.pageIndex * event.pageSize;
    this.highValue = this.lowValue + event.pageSize;
    return event;
  }

  onNewURL() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}

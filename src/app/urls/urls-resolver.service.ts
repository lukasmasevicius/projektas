import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DataStorageService } from '../shared/data-storage.service';
import { Url } from './url.model';
import { UrlService } from './url.service';

@Injectable({ providedIn: 'root' })
export class UrlsResolverService implements Resolve<Url[]> {
    constructor(
        private dataStorageService: DataStorageService,
        private urlService: UrlService
      ) {}

//: Observable<ObjectToResolve> | Promise<ObjectToResolve> | ObjectToResolve 
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        const urls = this.urlService.getURLs();

    if (urls.length === 0) {
      return this.dataStorageService.fetchURLs();
    } else {
      return urls;
    }
    }
}

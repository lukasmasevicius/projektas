import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Url } from '../url.model';
import { UrlService } from '../url.service';

@Component({
  selector: 'app-url-detail',
  templateUrl: './url-detail.component.html',
  styleUrls: ['./url-detail.component.css']
})
export class UrlDetailComponent implements OnInit {
  url!: Url;
  id: number = 0;

  constructor(private urlService: UrlService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.url = this.urlService.getURL(this.id);
        }
      );
  }



  onEditURL() {
    this.router.navigate(['edit'], {relativeTo: this.route});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onDeleteURL() {
    this.urlService.deleteURL(this.id);
    this.router.navigate(['/urls']);
  }

}

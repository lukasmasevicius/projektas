import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlStartComponent } from './url-start.component';

describe('UrlStartComponent', () => {
  let component: UrlStartComponent;
  let fixture: ComponentFixture<UrlStartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrlStartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

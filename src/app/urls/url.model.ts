
export class Url {
  public shortUrl: string;
  public originalPath: string;


  constructor(shortUrl: string, orgPath: string) {
    this.shortUrl = shortUrl;
    this.originalPath = orgPath;
  }
}

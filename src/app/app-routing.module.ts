
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { UrlDetailComponent } from './urls/url-detail/url-detail.component';
import { UrlEditComponent } from './urls/url-edit/url-edit.component';
import { UrlStartComponent } from './urls/url-start/url-start.component';
import { UrlsComponent } from './urls/urls.component';
import { UrlsResolverService } from './urls/urls-resolver.service';
import { AuthComponent } from './auth/auth.component';


const routes: Routes = [
  { path: '', redirectTo: '/urls', pathMatch: 'full' },
  {
    path: 'urls',
    component: UrlsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: UrlStartComponent },
      { path: 'new', component: UrlEditComponent },
      {
        path: ':id',
        component: UrlDetailComponent,
        resolve: [UrlsResolverService]
      },
      {
        path: ':id/edit',
        component: UrlEditComponent,
        resolve: [UrlsResolverService]
      }
    ]
  },
  { path: 'auth', component: AuthComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
